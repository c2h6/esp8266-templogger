#include <OneWire.h>
#include <DallasTemperature.h>

// data wire is connected with GPIO5/D1:
#define ONE_WIRE_BUS 4

// setup one wire instance:
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

void setup()
{
  // initialize DS1820 sensor with 12 bit resolution:
  sensors.begin();
  sensors.setResolution(12);
  Serial.begin(115200);
}

int sensorCount;

void loop()
{
  delay(1000);
  sensors.requestTemperatures();
  sensorCount = sensors.getDS18Count();
  for (int i=0; i<sensorCount; i++) {
    Serial.print("Index: " + String(i) + " Temp: " + String(sensors.getTempCByIndex(i))+ "\t");
  }
  Serial.println();
}

/*
void updateMyCloud() {
    if(!client.connected()){
      client.connect("c2h6.eu",8086);
    }
    String postStr = "TEMP1 ";
    postStr +="temp1="+String(temp1);
    postStr += "\n\n";
 
    client.print("POST /write?db=tempSensors HTTP/1.1\n");
    client.print("Host: c2h6.eu\n");
    client.print("Connection: close\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("User-Agent: Ethan's ESP8266\n");
    client.print("Content-Length: ");
    client.print(postStr.length());
    client.print("\n\n");
    client.print(postStr);
    client.stop();
}
*/